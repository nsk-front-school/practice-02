## 02 - практика - размещение элементов

## Начало работы

- ознакомься с инструкцией по работе с репозиторием https://gitlab.com/nsk-front-school/rentbike в файле [readme](https://gitlab.com/nsk-front-school/rentbike/-/blob/main/README.md)
- создай дочернюю ветку для задания
- после выполнения задания не забудь создать мерж-реквест и озаглавить его `02 - практика - размещение элементов`

## Задание

Сегодня в качестве практического занятия будем верстать формы, используя стандартные средства HTML.
Для каждой формы создай компонент в `storybook`

- [Форма авторизации](https://www.figma.com/file/ZXpD8WcDKMNkqKMHcRlahi/%D0%A8%D0%BA%D0%BE%D0%BB%D0%B0-%D1%84%D1%80%D0%BE%D0%BD%D1%82%D0%B0?node-id=7%3A4096)
- [Форма регистрации](https://www.figma.com/file/ZXpD8WcDKMNkqKMHcRlahi/%D0%A8%D0%BA%D0%BE%D0%BB%D0%B0-%D1%84%D1%80%D0%BE%D0%BD%D1%82%D0%B0?node-id=7%3A4404)
- [Форма ввода реквизитов карты](https://www.figma.com/file/ZXpD8WcDKMNkqKMHcRlahi/%D0%A8%D0%BA%D0%BE%D0%BB%D0%B0-%D1%84%D1%80%D0%BE%D0%BD%D1%82%D0%B0?node-id=8%3A2251)
- сверстай компоненты формы в соответсвии с прототипами
- для разных состояний каждой из форм создавай отдельные HTML-файлы
- для кнопок и ссылок используй уже существующие компоненты
- для валидации формы используй средства которые предоставляет HTML. [Подробнее](https://developer.mozilla.org/ru/docs/Learn/Forms/Form_validation) 
